import pygame

class IzzieTDD():
    #see player.py from miner package 
    
    def __init__(self):
        
        self.image = pygame.image.load("D:\Projects\EclipseProjects\Mazeltov\littleIzzie3.png")
        self.rect = self.image.get_rect()
        self.yVel = 0
        self.xVel = 0
        
        
        self.jumping = False
        self.onGround = False
        self.doubleJumping = False
        self.origJumpVel = 10
        self.jumpVel = self.origJumpVel
        self.gravity = 0.5
        
    def doJump(self):
        if self.jumping and not self.onGround:
            self.yVel = -self.jumpVel
            self.jumpVel -= self.gravity
            if self.doubleJumping:
                self.jumpVel = self.origJumpVel
                self.doubleJumping = False
                
        if self.onGround:
            self.jumping = False
            self.jumpVel = self.origJumpVel
            self.yVel = 0
            self.onGround = True

izzie = IzzieTDD()