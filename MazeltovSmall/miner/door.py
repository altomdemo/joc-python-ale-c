import pygame

from miner.constants import *
from miner.data import filepath

global DOOR_X
global DOOR_Y
DOOR_X = 0
DOOR_Y = 0

class DoorSprite(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        
        pygame.display.init()
        pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

        #self.image = pygame.Surface([BLOCK_W/1.5, BLOCK_H]) # make the player sprite a little taller than the blocks, but NOT the rect
        self.image = pygame.image.load(filepath('door2.png')).convert_alpha()
        
        self.door = pygame.Surface([BLOCK_W, BLOCK_H])
        #self.door.fill(DOOR_COLOR)

        #self.rect = self.image.get_rect()
        self.rect = pygame.Rect((0, 0, BLOCK_W, BLOCK_H))
        self.rect.x = 10
        self.rect.y = 250     
        
    def setXY(self, doorX, doorY):
        global DOOR_X
        global DOOR_Y
        DOOR_X = doorX
        DOOR_Y = doorY
        
    def getX(self):
        global DOOR_X
        return DOOR_X
    
    def getY(self):
        global DOOR_Y
        return DOOR_Y
        
    def draw(self, surface):
        surface.blit(self.image, (self.rect.x, self.rect.y))

    def reset(self):
        global DOOR_X
        global DOOR_Y
        
        self.rect.x = DOOR_X
        self.rect.y = DOOR_Y

# define door
door = DoorSprite()

class BackgroundSprite(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        
        pygame.display.init()
        pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

        #self.image = pygame.Surface([BLOCK_W/1.5, BLOCK_H]) # make the player sprite a little taller than the blocks, but NOT the rect
        self.image = pygame.image.load(filepath('background.png'))
        
        self.background = pygame.Surface([SCREEN_WIDTH, SCREEN_HEIGHT])
        #self.door.fill(DOOR_COLOR)

        #self.rect = self.image.get_rect()
        self.rect = pygame.Rect((0, 0, SCREEN_WIDTH, SCREEN_HEIGHT))
        
        
    def draw(self, surface):
        surface.blit(self.image, (self.rect.x, self.rect.y))

# define door
background = BackgroundSprite()