from miner.engine import GameEngine
from miner.constants import *

def main():
    gameEngine = GameEngine(SCREEN_WIDTH, SCREEN_HEIGHT)
    gameEngine.initializeGame()
    