import pygame
import time

from miner.menus import *
from miner.player import player
from miner.door import door
from miner.level import level
from miner.graphics import GraphicsEngine
from miner.constants import *

global playerIsFlipped
playerIsFlipped = False

class GameEngine():
    def __init__(self, screenWidth, screenHeight):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight

        # screen
        self.screen = None

        # menu state
        self.GAME_STATE = MENU_STARTGAME

        # game
        self.clock = pygame.time.Clock()
        self.gameTime = time.time()
        self.gameRunning = True
        self.currentLevel = 1

    def initializeGame(self):
        pygame.init()
        self.screen = pygame.display.set_mode([self.screenWidth, self.screenHeight])
        pygame.display.set_caption('Mazeltov')

        # engines
        self.graphicsEngine = GraphicsEngine(self.screen)

        # menus
        self.menuStartGame = MenuStartGame(self.screen, self)
        self.menuAbout = MenuAbout(self.screen, self)
        self.menuScene = MenuScene(self.screen, self)
        self.menuGameOver = MenuGameOver(self.screen, self)
        self.menuGameFinish = MenuGameFinish(self.screen, self)

        # start the loop
        self.gameLoop()

    def gameLoop(self):
        while self.gameRunning:
            if self.GAME_STATE == MENU_STARTGAME:
                self.menuStartGame.draw()
                self.menuStartGame.update()

            elif self.GAME_STATE == MENU_ABOUT:
                self.menuAbout.draw()
                self.menuAbout.update()

            elif self.GAME_STATE == MENU_SCENE:
                self.menuScene.draw()
                self.menuScene.update()

            elif self.GAME_STATE == MENU_GAMEOVER:
                self.menuGameOver.draw()
                self.menuGameOver.update()

            elif self.GAME_STATE == MENU_GAMEFINISH:
                self.menuGameFinish.draw()
                self.menuGameFinish.update()

            elif self.GAME_STATE == MENU_INGAME:
                # handle input
                pygame.event.pump()
                for event in pygame.event.get():
                    self.handleInput(event)

                    if event.type == pygame.QUIT:
                        self.gameRunning = False
                        break

                # update everything
                self.playerMove()

                self.checkGameState()

                # draw everything
                self.graphicsEngine.renderGame()
                pygame.display.update()

                # limit fps
                self.clock.tick(60)

        # game not running any longer, so lets quit
        pygame.quit()

    def handleInput(self, event):
        global player

        def pressed(key):
            keys = pygame.key.get_pressed()

            if keys[key]:
                return True
            else:
                return False
       
        if event.type == pygame.KEYDOWN:

            if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                player.xVel = -MOVEMENT_SPEED
                player.direction = DIR_LEFT

            elif event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                player.xVel = +MOVEMENT_SPEED
                player.direction = DIR_RIGHT

            # jump
            elif event.key == pygame.K_SPACE  :
                if player.jumping == True:
                    player.doubleJumping = True                
                player.jumping = True
                player.onGround = False
                
            elif event.key == pygame.K_r  :          
                self.setState(MENU_SCENE)

            # exit
            elif event.key == pygame.K_q or event.key == pygame.K_ESCAPE:
                self.gameRunning = False

        elif event.type == pygame.KEYUP:

            if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                player.xVel += MOVEMENT_SPEED
                player.direction = DIR_LEFT


            elif event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                player.xVel -= MOVEMENT_SPEED
                player.direction = DIR_RIGHT




    def setState(self, state):
        if state == MENU_INGAME:
            # reset game information
            level.generateLevel()

            player.reset()
            door.reset()
            

        self.GAME_STATE = state

    def resetGame(self):
        self.currentLevel = 1
        self.menuScene.currentScene = 0

        level.generateLevel()
        print('resetting player')
        player.reset()
        door.reset()

    def checkGameState(self):
        theDoorX = door.getX()
        theDoorY = door.getY()

        if player.rect.x < theDoorX + 10 and player.rect.x > theDoorX - 10:
            if player.rect.y < theDoorY + 10 and player.rect.y > theDoorY - 10:
                self.currentLevel += 1
                self.setState(MENU_SCENE)

    def checkCollision(self, sprite, xVel, yVel):
        for x in range(len(level.levelStructure)):
            for y in range(len(level.levelStructure[x])):
                block = level.levelStructure[x][y]

                if block is not None:
                    if pygame.sprite.collide_rect(sprite, block):
                        if xVel < 0:
                            sprite.rect.x = block.rect.x + block.rect.w

                        if xVel > 0:
                            sprite.rect.x = block.rect.x - sprite.rect.w

                        if yVel < 0:
                            sprite.rect.y = block.rect.y + block.rect.h

                        if yVel > 0 and not sprite.onGround:
                            sprite.onGround = True
                            sprite.rect.y = block.rect.y - sprite.rect.h


    def playerMove(self):
        global playerIsFlipped

        player.doJump()        
       
        if player.direction == 2 and playerIsFlipped == False:
            player.image = pygame.transform.flip(player.image, True, False)
            playerIsFlipped = True;
        elif player.direction == 3  and playerIsFlipped == True:
            player.image = pygame.transform.flip(player.image, True, False)
            playerIsFlipped = False

        player.onGround = False
        if not player.onGround and not player.jumping:
            player.yVel += 1
            #player.yVel = -player.jumpVel

        if player.xVel > MOVEMENT_SPEED:
            player.xVel = MOVEMENT_SPEED
        elif player.xVel < -MOVEMENT_SPEED:
            player.xVel = -MOVEMENT_SPEED

        if player.rect.x <= 0:
            player.rect.x = 0
        elif player.rect.x >= SCREEN_WIDTH - player.rect.w:
            player.rect.x = SCREEN_WIDTH - player.rect.w

        # move player and check collision
        player.rect.x += player.xVel
        self.checkCollision(player, player.xVel, 0)
        player.rect.y += player.yVel
        self.checkCollision(player, 0, player.yVel)
