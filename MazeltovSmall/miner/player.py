import pygame

from miner.constants import *
from miner.data import filepath

global PLAYER_START_X 
global PLAYER_START_Y

PLAYER_START_X = 0
PLAYER_START_Y = 0

class PlayerSprite(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)        
        pygame.display.init()
        pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

        self.image = pygame.image.load(filepath('littleIzzie5.png')).convert_alpha()
        self.rect = pygame.Rect((0, 0, BLOCK_W, BLOCK_H/2))
        self.xVel = 0
        self.yVel = 0

        # jumping
        self.jumping = False
        self.doubleJumping = False
        self.onGround = False
        self.origJumpVel = 10
        self.jumpVel = self.origJumpVel
        self.gravity = 0.5
        
    def setXY(self, x, y):
        global PLAYER_START_X
        global PLAYER_START_Y
        PLAYER_START_X = x
        PLAYER_START_Y = y
        
    def draw(self, surface):
        y = self.rect.y + (self.rect.h - self.image.get_rect().h)
        surface.blit(self.image, (self.rect.x, y))    

    def reset(self):
        global PLAYER_START_X
        global PLAYER_START_Y

        self.rect.x = PLAYER_START_X
        self.rect.y = PLAYER_START_Y

        self.xVel = 0
        self.yVel = 0
        self.direction = DIR_RIGHT

        self.collectedResources = 0


    def doJump(self):

        if self.jumping and not self.onGround:            
            self.yVel = -self.jumpVel
            self.jumpVel -= self.gravity
            if self.doubleJumping:
                self.jumpVel = self.origJumpVel
                self.doubleJumping = False

        if self.onGround:
            self.jumping = False
            self.jumpVel = self.origJumpVel
            self.yVel = 0
            self.onGround = True

# define player
player = PlayerSprite()
