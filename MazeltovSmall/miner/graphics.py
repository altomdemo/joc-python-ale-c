import pygame
import random
import math

from miner.player import player
from miner.door import door, background
from miner.level import level
from miner.data import filepath
from miner.constants import *

class GraphicsEngine():
    def __init__(self, surface):
        self.screenSurface = surface

        # sprite groups
        self.allSprites = pygame.sprite.Group()
        self.allSprites.add(player)
        self.allSprites.add(door)
        self.allSprites.add(background)

        # load everything
        self.loadResources()

    def loadResources(self):

        # fonts
        self.scoreFont = pygame.font.Font(filepath('Minecraftia.ttf'), 16)

    def renderGame(self):   
        
        self.screenSurface.fill((0, 0, 0))
        background.draw(self.screenSurface)
        # draw blocks
        for x in range(100):
            for y in range(100):
                block = level.levelStructure[x][y]
                if block != None:
                    block.draw(self.screenSurface)

        player.draw(self.screenSurface)
        door.draw(self.screenSurface)
        
        ## A black mask for the screen.
        mask = pygame.surface.Surface([SCREEN_WIDTH, SCREEN_HEIGHT]).convert_alpha()

        mask.fill((0,0,0,255))       

        
        radius = 155
        t = 255
        delta = 3
        while radius > 10:
            pygame.draw.circle(mask,(0,0,0,t),[player.rect.x + math.floor(BLOCK_W/2), player.rect.y], radius)
            t -= delta
            radius -= delta
        pygame.draw.circle(mask,(0,0,0,0),[player.rect.x + math.floor(BLOCK_W/2), player.rect.y], radius)

        ## Cover the screen with the partly-translucent mask
            
        self.screenSurface.blit(mask,(0,0))

