import pygame

from miner.data import filepath
from miner.constants import *

class BlockSprite(pygame.sprite.Sprite):
    def __init__(self, x, y, isResourceRich=False):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.Surface([BLOCK_W, BLOCK_H])
        self.image = pygame.image.load(filepath('block.jpg'))
        
        #self.image.fill(BLOCK_COLOR)

        self.isBorder = False
        self.borderImage = pygame.Surface([BLOCK_W, BLOCK_H])


        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        self.borders = [False, False, False, False]

    def draw(self, surface):
        
        surface.blit(self.image, self.rect)

