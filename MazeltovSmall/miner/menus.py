import pygame
import sys

from miner.data import filepath
from miner.constants import *

# menu object
class MenuClass():
    def __init__(self, surface):
        self.surface = surface

        self.backgroundColor = (0, 0, 0)

        # font
        self.textFont = pygame.font.Font(filepath('Minecraftia.ttf'), 12)

        # menus
        self.selectedMenu = 0
        self.menus = []

        # texts
        self.texts = []

        # keyboard events
        self.keyboardEvents = []

    def draw(self):
        self.surface.fill(self.backgroundColor)

        if len(self.menus) != 0:
            self.drawMenus()

        # update surface
        pygame.display.update()

    def drawMenus(self):
        for menu in self.menus:
            menu.draw(self.surface)

    def update(self):
        for event in pygame.event.get():
            self._handleEvents(event)

    def _handleEvents(self, event):
        def pressed(key):
            keys = pygame.key.get_pressed()

            if keys[key]:
                return True
            else:
                return False

        if pressed(pygame.K_UP):
            if self.selectedMenu <= 0:
                self.selectedMenu = len(self.menus) - 1
            else:
                self.selectedMenu -= 1

            self._updateSelectedMenu()

        if pressed(pygame.K_DOWN):
            if self.selectedMenu >= (len(self.menus) - 1):
                self.selectedMenu = 0
            else:
                self.selectedMenu += 1

            self._updateSelectedMenu()

        if pressed(pygame.K_SPACE) or pressed(pygame.K_RETURN) or pressed(pygame.K_x):
            if len(self.menus) != 0:
                self.menus[self.selectedMenu].click()

        if pressed(pygame.K_q) or pressed(pygame.K_ESCAPE):
            pygame.quit()
            sys.exit()

    def addMenu(self, caption, fgColor=(255,255,255)):
        self.menus.append(MenuItemClass(caption, fgColor=fgColor))

        # update placement (center)
        menuAmount = len(self.menus)
        for i in range(len(self.menus)):
            self.menus[i].rect.centerx = SCREEN_WIDTH / 2
            self.menus[i].rect.centery = SCREEN_HEIGHT / (menuAmount + 1) * (i+1)

        self._updateSelectedMenu()

    def _updateSelectedMenu(self):
        for menu in self.menus:
            if self.menus.index(menu) == self.selectedMenu:
                menu.select()
            else:
                menu.deselect()


class MenuItemClass(pygame.sprite.Sprite):
    def __init__(self, caption='', antialias=0, fgColor=(255,255,255)):
        pygame.sprite.Sprite.__init__(self)

        self.caption = caption
        self.antialias = antialias
        self.fgColor = fgColor

        self.font = pygame.font.Font(filepath('Minecraftia.ttf'), 32)

        self.image = self.font.render(self.caption, antialias, fgColor)
        self.rect = self.image.get_rect()

        self.event = None

    def draw(self, surface):
        surface.blit(self.image, self.rect)

    def select(self):
        self.image = self.font.render(self.caption, self.antialias, (100, 100, 100))

    def deselect(self):
        self.image = self.font.render(self.caption, self.antialias, self.fgColor)

    def click(self):
        if self.event != None:
            self.event()

    def connect(self, action):
        self.event = action

# menus
class MenuStartGame(MenuClass):
    def __init__(self, surface, engine):
        MenuClass.__init__(self, surface)

        self.engine = engine

        self.addMenu('Start')
        self.menus[0].connect(self.clickStart)

        self.addMenu('About')
        self.menus[1].connect(self.clickAbout)

        self.addMenu('Quit')
        self.menus[2].connect(self.clickQuit)

    def clickStart(self):
        self.engine.GAME_STATE = MENU_SCENE

    def clickAbout(self):
        self.engine.setState(MENU_ABOUT)

    def clickQuit(self):
        pygame.quit()
        sys.exit()


class MenuScene(MenuClass):
    def __init__(self, surface, engine):
        MenuClass.__init__(self, surface)

        self.engine = engine

        self.currentScene = 0

        self.scenes = ['Izzie is a curios little girl...', \
        'This time she got herself into a bit of a mess.\n She is trapped inside a maze with not one soul to help her.\n\n', \
        'Use your pattern matching capabilities to guide her towards the exit door in time for supper.\n Her mother made Chickpea Soup with Garlic & Unions\n Altom Team Assemble!\n\n']


    def draw(self):
        self.surface.fill(self.backgroundColor)

        self.renderText()
        self.drawHint()

        # update surface
        pygame.display.update()

    def _handleEvents(self, event):
        def pressed(key):
            keys = pygame.key.get_pressed()

            if keys[key]:
                return True
            else:
                return False

        if pressed(pygame.K_x):
            self.nextScene()

    def nextScene(self):
        # 2 represents the number of boring text scenes
        if self.currentScene < 2:
            # li
            self.currentScene += 1
        elif self.currentScene == 9:
            self.engine.resetGame()
            self.engine.setState(MENU_GAMEFINISH)
        else:
            self.engine.setState(MENU_INGAME)

    def renderText(self):
        # split texts at \n (newline)

        texts = self.scenes[self.currentScene].split('\n')

        for i in range(len(texts)):
            textSurface = self.textFont.render(texts[i], 0, (255, 255, 255))

            textRect = textSurface.get_rect()
            textRect.centerx = SCREEN_WIDTH / 2
            textRect.centery = SCREEN_HEIGHT / 2 + i * self.textFont.size(texts[i])[1]

            self.surface.blit(textSurface, textRect)

    def drawHint(self):
        textSurface = self.textFont.render('(Click X to continue)', 0, (255, 255, 255))
        textRect = textSurface.get_rect()
        textRect.centerx = SCREEN_WIDTH/2
        textRect.centery = SCREEN_HEIGHT - 50
        self.surface.blit(textSurface, textRect)

class MenuAbout(MenuClass):
    def __init__(self, surface, engine):
        MenuClass.__init__(self, surface)

        self.engine = engine

        self.currentScene = 0

        self.text = 'Game created by Ale C. for Altom as an internal project\n Some of the credits go to Marcus Moller''s project called Miner [nmarcusmoller @ GitHub] \n which served as a reference for harder programming techniques. \nAlso the algorithm used for the Maze Generator is based on one made by Joe Wingbermuehle.'

    def draw(self):
        self.surface.fill(self.backgroundColor)

        self.renderText()
        self.drawHint()

        # update surface
        pygame.display.update()

    def _handleEvents(self, event):
        def pressed(key):
            keys = pygame.key.get_pressed()

            if keys[key]:
                return True
            else:
                return False

        if pressed(pygame.K_ESCAPE):
            self.engine.setState(MENU_STARTGAME)

    def renderText(self):
        # split texts at \n (newline)

        texts = self.text.split('\n')

        for i in range(len(texts)):
            textSurface = self.textFont.render(texts[i], 0, (255, 255, 255))

            textRect = textSurface.get_rect()
            textRect.centerx = SCREEN_WIDTH / 2
            textRect.centery = SCREEN_HEIGHT / 2 + i * self.textFont.size(texts[i])[1]

            self.surface.blit(textSurface, textRect)

    def drawHint(self):
        textSurface = self.textFont.render('(Click ESC to return)', 0, (255, 255, 255))
        textRect = textSurface.get_rect()
        textRect.centerx = SCREEN_WIDTH/2
        textRect.centery = SCREEN_HEIGHT - 50
        self.surface.blit(textSurface, textRect)

class MenuGameOver(MenuClass):
    def __init__(self, surface, engine):
        MenuClass.__init__(self, surface)

        self.engine = engine

        self.text = '... Sam.\nYou failed.\n\nYou will never make it back to Earth now.'

    def draw(self):
        self.surface.fill(self.backgroundColor)

        self.renderText()
        self.drawHint()

        # update surface
        pygame.display.update()

    def _handleEvents(self, event):
        def pressed(key):
            keys = pygame.key.get_pressed()

            if keys[key]:
                return True
            else:
                return False

        if pressed(pygame.K_x):
            self.engine.setState(MENU_STARTGAME)

    def renderText(self):
        # split texts at \n (newline)

        texts = self.text.split('\n')

        for i in range(len(texts)):
            textSurface = self.textFont.render(texts[i], 0, (255, 255, 255))

            textRect = textSurface.get_rect()
            textRect.centerx = SCREEN_WIDTH / 2
            textRect.centery = SCREEN_HEIGHT / 2 + i * self.textFont.size(texts[i])[1]

            self.surface.blit(textSurface, textRect)

    def drawHint(self):
        textSurface = self.textFont.render('(Click X to return to main screen)', 0, (255, 255, 255))
        textRect = textSurface.get_rect()
        textRect.centerx = SCREEN_WIDTH/2
        textRect.centery = SCREEN_HEIGHT - 50
        self.surface.blit(textSurface, textRect)

class MenuGameFinish(MenuClass):
    def __init__(self, surface, engine):
        MenuClass.__init__(self, surface)

        self.engine = engine

        self.text = 'GAME COMPLETED\nThanks for playing MINER, my PyWeek #17 entry!'

    def draw(self):
        self.surface.fill(self.backgroundColor)

        self.renderText()
        self.drawHint()

        # update surface
        pygame.display.update()

    def _handleEvents(self, event):
        def pressed(key):
            keys = pygame.key.get_pressed()

            if keys[key]:
                return True
            else:
                return False

        if pressed(pygame.K_x):
            self.engine.setState(MENU_STARTGAME)

    def renderText(self):
        # split texts at \n (newline)

        texts = self.text.split('\n')

        for i in range(len(texts)):
            textSurface = self.textFont.render(texts[i], 0, (255, 255, 255))

            textRect = textSurface.get_rect()
            textRect.centerx = SCREEN_WIDTH / 2
            textRect.centery = SCREEN_HEIGHT / 2 + i * self.textFont.size(texts[i])[1]

            self.surface.blit(textSurface, textRect)

    def drawHint(self):
        textSurface = self.textFont.render('(Click X to return to main screen)', 0, (255, 255, 255))
        textRect = textSurface.get_rect()
        textRect.centerx = SCREEN_WIDTH/2
        textRect.centery = SCREEN_HEIGHT - 50
        self.surface.blit(textSurface, textRect)
