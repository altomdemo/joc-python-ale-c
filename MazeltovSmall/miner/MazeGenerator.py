# Pygcurse Maze
# By Al Sweigart al@inventwithpython.com
# Maze Generation code by Joe Wingbermuehle

# This program is a demo for the Pygcurse module.
# Simplified BSD License, Copyright 2011 Al Sweigart

import random
from pygame.locals import *

MAZE_WIDTH  = 41
MAZE_HEIGHT = 41


class JoeWingMaze():
    # Maze generator in Python
    # Joe Wingbermuehle
    # 2010-10-06
    # http://joewing.net/programs/games/python/maze.py

    def __init__(self, width=21, height=21):
        if width % 2 == 0:
            width += 1
        if height % 2 == 0:
            height += 1

        # The size of the maze (must be odd).
        self.width  = width
        self.height = height

        # The maze.
        self.maze = dict()

        # Generate and display a random maze.
        self.init_maze()
        self.generate_maze()
        #self.display_maze() # prints out the maze to stdout


    # Initialize the maze.
    def init_maze(self):
        for x in range(0, self.width):
            self.maze[x] = dict()
            for y in range(0, self.height):
                self.maze[x][y] = 1

    # Carve the maze starting at x, y.
    def carve_maze(self, x, y):
        dir = random.randint(0, 3)
        count = 0
        while count < 4:
            dx = 0
            dy = 0
            if   dir == 0:
                dx = 1
            elif dir == 1:
                dy = 1
            elif dir == 2:
                dx = -1
            else:
                dy = -1
            x1 = x + dx
            y1 = y + dy
            x2 = x1 + dx
            y2 = y1 + dy
            if x2 > 0 and x2 < self.width and y2 > 0 and y2 < self.height:
                if self.maze[x1][y1] == 1 and self.maze[x2][y2] == 1:
                    self.maze[x1][y1] = 0
                    self.maze[x2][y2] = 0
                    self.carve_maze(x2, y2)
            count = count + 1
            dir = (dir + 1) % 4

    # Generate the maze.
    def generate_maze(self):
        random.seed()
        #self.maze[1][1] = 0
        self.carve_maze(1, 1)
        #self.maze[1][0] = 0
        #self.maze[self.width - 2][self.height - 1] = 0

        # maze generator modified to have randomly placed entrance/exit.
        startx = starty = endx = endy = 0
        while self.maze[startx][starty]:
            startx = random.randint(1, self.width-2)
            starty = random.randint(1, self.height-2)
        while self.maze[endx][endy] or endx == 0 or abs(startx - endx) < int(self.width / 3) or abs(starty - endy) < int(self.height / 3):
            endx = random.randint(1, self.width-2)
            endy = random.randint(1, self.height-2)

        self.maze[startx][starty] = 0
        self.maze[endx][endy] = 0

        self.startx = startx
        self.starty = starty
        self.endx = endx
        self.endy = endy
