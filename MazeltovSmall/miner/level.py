import pygame

from miner.block import BlockSprite
from miner.constants import BLOCK_W, BLOCK_H
from miner.player import PlayerSprite
from miner.door import DoorSprite
from miner.MazeGenerator import JoeWingMaze

class LevelEngine():
    def __init__(self):
        self.blocks = pygame.sprite.Group()
        # player
        self.blocksAroundPlayer = []

    def generateLevel(self):       
        
        self.levelStructure = [[None for i in range(100)] for i in range(100)]
        
        jwmaze = JoeWingMaze(31, 31)
        maze = jwmaze.maze
        yOffset = 0
        xOffset = 5
        
                
        myX = (jwmaze.startx + xOffset) * BLOCK_W
        myY = (jwmaze.starty + yOffset) * BLOCK_H
        doorkX = (jwmaze.endx + xOffset) * BLOCK_W
        doorkY = (jwmaze.endy + yOffset) * BLOCK_H
        
        PlayerSprite.setXY(self, myX, myY )
        DoorSprite.setXY(self, doorkX, doorkY)
        
        for x in range(31):
            for y in range(31):
                if(maze[x][y] != 0):  
                 
                    tempBlock = BlockSprite((x+xOffset)*BLOCK_W, (y+yOffset)*BLOCK_H)

                    self.blocks.add(tempBlock)
                    self.levelStructure[x][y] = tempBlock
                else:
                    continue    

level = LevelEngine()
