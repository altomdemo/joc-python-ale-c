import unittest
import DoorTDD

class TestDoor(unittest.TestCase):
    
    def setUp(self):
        self.doorTDD = DoorTDD.DoorTDD()

    def testDoorHasImage(self):        
        self.assertNotEqual(self.doorTDD.image != None, "doorTDD image is None")

    def testDoorHasRectangle(self):        
        self.assertNotEqual(self.doorTDD.rect != None, "doorTDD rectangle is None")
        
    def testDoorHasX(self):
        self.assertNotEqual(self.doorTDD.X != None, "doorTDD X is None")      
        
    def testDoorHasY(self):
        self.assertNotEqual(self.doorTDD.Y != None, "doorTDD Y is None")  
        
    def testDoorJumps(self):
        self.assertNotEqual(self.doorTDD.DoJump() != None, "doors don't jump")       
         

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()