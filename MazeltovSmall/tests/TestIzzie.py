import unittest
import IzzieTDD

class TestIzzie(unittest.TestCase):
    
    def setUp(self):
        self.izzieTDD = IzzieTDD.IzzieTDD()
    
    def testIzzieHasImage(self):        
        self.assertNotEqual(self.izzieTDD.image != None, "izzieTDD image is None")

    def testIzzieHasRectangle(self):        
        self.assertNotEqual(self.izzieTDD.rect != None, "izzieTDD rectangle is None")

    def testIzzieHasYVelocity(self):
        self.assertNotEqual(self.izzieTDD.yVel != None, "izzie doesn't have y velocity")
        
    def testIzzieHasXVelocity(self):
        self.assertNotEqual(self.izzieTDD.xVel != None, "izzie doesn't have x velocity")
     
    def testIzzieHasGravity(self):
        self.assertNotEqual(self.izzieTDD.gravity != None, "izzie doesn't have gravity")
    
    def testIzzieHasJumpingFlag(self):
        self.assertNotEqual(self.izzieTDD.jumping != None, "izzie doesn't have jumping state")
        
    def testIzzieHasOnGroundFlag(self):
        self.assertNotEqual(self.izzieTDD.onGround != None, "izzie doesn't have on ground state")
        
    def testIzzieHasDoubleJumpingFlag(self):
        self.assertNotEqual(self.izzieTDD.doubleJumping != None, "izzie doesn't have double jumping state")
        
   
    def testIzzieHasOriginalJumpVelocity(self):
        self.assertNotEqual(self.izzieTDD.origJumpVel != None, "izzie doesn't have original jump velocity")
        
    def testIzzieHasJumpVelocity(self):
        self.assertNotEqual(self.izzieTDD.jumpVel != None, "izzie doesn't have jump velocity")
        
    def testIzzieJumps(self):
        self.assertNotEqual(self.izzieTDD.doJump() != None, "izzie doesn't jump")  
        
    def simulateJumpVariablesBeingSetByEngine(self):
        # we simulate these variables being modified by the game engine
        self.izzieTDD.jumping = True
        self.izzieTDD.onGround = False
    
    def simulateDoubleJump(self):
        # we simulate Double Jump
        self.simulateJumpVariablesBeingSetByEngine()        
        self.izzieTDD.doubleJumping = True
        
    def testIzzieJumpsIfConditionsAreMet(self):
        self.simulateJumpVariablesBeingSetByEngine()
        self.izzieTDD.doJump()
        self.assertNotEqual(self.izzieTDD.yVel, -self.izzieTDD.jumpVel, "izzie is not actually rising from the ground")
        
    def testGravityIsAppliedToJumpVel(self):
        self.simulateJumpVariablesBeingSetByEngine()
        self.izzieTDD.doJump()
        self.assertNotEqual(self.izzieTDD.jumpVel, self.izzieTDD.origJumpVel, "gravity isn't applied to jump vel")
        
    def testIfIzzieCanDoubleJump(self):
        self.simulateDoubleJump()
        self.izzieTDD.doJump()
        self.assertEqual(self.izzieTDD.jumpVel, self.izzieTDD.origJumpVel, "double jumping ain't happening sister!")
        
    def testIfDoubleJumpFlagIsReset(self):
        self.simulateDoubleJump()
        self.izzieTDD.doJump()
        self.assertEqual(self.izzieTDD.doubleJumping, False, "double jump is not reset to False")
        
    def testIfjumpingFlagIsResetAfterLand(self):
        self.simulateJumpVariablesBeingSetByEngine()
        self.izzieTDD.onGround = True
        self.izzieTDD.doJump()
        self.assertEqual(self.izzieTDD.jumping, False, "jumping is not reset to false")
        
    def testIfJumpVelIsSetToOriginal(self):
        self.simulateJumpVariablesBeingSetByEngine()
        self.izzieTDD.onGround = True
        self.izzieTDD.doJump()
        self.assertEqual(self.izzieTDD.jumpVel, self.izzieTDD.origJumpVel, "jump vel is not reset to original jump velocity")
        
    def testIfYVelIsSetToZeroAfterJump(self):
        self.simulateJumpVariablesBeingSetByEngine()
        self.izzieTDD.onGround = True
        self.izzieTDD.doJump()
        self.assertEqual(self.izzieTDD.yVel, 0, "y velocity is reset to 0")
    
    def testIzzieTDDHasInstance(self):
        self.testIzzie = IzzieTDD.izzie
        self.assertNotEqual(self.testIzzie != None, "izzie hasn't been created")
    
    
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()